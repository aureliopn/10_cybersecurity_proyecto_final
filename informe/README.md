# pentest-report-template
This template was crated for penetration testers who love working with LaTeX and understand its true power when it comes to creating beautiful PDF files. I personally used it to pass the eWPT exam and in my daily work.
## Usage
Clone this repository and open `document.tex` in your TexStudio.  
After compiling it should look like in the `document.pdf` file.  
P.S. The report looks much better when there's some data in it.
## Contributions
If you'd like to make this template better you are more than welcome to create a pull request.