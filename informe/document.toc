\babel@toc {english}{}\relax 
\contentsline {chapter}{\numberline {1}Informe Ejecutivo}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Resumen de vulnerabilidades}{2}{section.1.1}%
\contentsline {chapter}{\numberline {2}Reconocimiento (Footprinting)}{3}{chapter.2}%
\contentsline {section}{\numberline {2.1}Lanzamiento de ReconFTW}{3}{section.2.1}%
\contentsline {section}{\numberline {2.2}Footprinting Vertical y Horizontal}{3}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Servicios a la escucha}{4}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Tecnologías}{5}{subsection.2.2.2}%
\contentsline {subsection}{\numberline {2.2.3}Web Crawling}{6}{subsection.2.2.3}%
\contentsline {subsection}{\numberline {2.2.4}Tecnologías Web}{7}{subsection.2.2.4}%
\contentsline {section}{\numberline {2.3}OSINT}{8}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}Enlaces a repositorio}{8}{subsection.2.3.1}%
\contentsline {chapter}{\numberline {3}Resultados}{9}{chapter.3}%
\contentsline {section}{\numberline {3.1}Vulnerabilidades a nivel de infraestructura}{9}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Inyección de Comandos y Escalada de privilegios}{9}{subsection.3.1.1}%
\contentsline {subsubsection}{\numberline {3.1.1.1}Obtención}{9}{subsubsection.3.1.1.1}%
\contentsline {subsubsection}{\numberline {3.1.1.2}Recomendaciones}{9}{subsubsection.3.1.1.2}%
\contentsline {subsubsection}{\numberline {3.1.1.3}Información}{10}{subsubsection.3.1.1.3}%
\contentsline {section}{\numberline {3.2}Vulnerabilidades Web}{10}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}Denegación de Servicio}{11}{subsection.3.2.1}%
\contentsline {subsubsection}{\numberline {3.2.1.1}Obtención}{11}{subsubsection.3.2.1.1}%
\contentsline {subsubsection}{\numberline {3.2.1.2}Descripción}{11}{subsubsection.3.2.1.2}%
\contentsline {subsubsection}{\numberline {3.2.1.3}Recomendaciones}{11}{subsubsection.3.2.1.3}%
\contentsline {subsubsection}{\numberline {3.2.1.4}Enlaces}{11}{subsubsection.3.2.1.4}%
\contentsline {subsection}{\numberline {3.2.2}Cross-Site Request Forgery (CSRF)}{12}{subsection.3.2.2}%
\contentsline {subsubsection}{\numberline {3.2.2.1}Obtención}{12}{subsubsection.3.2.2.1}%
\contentsline {subsubsection}{\numberline {3.2.2.2}Descripción}{12}{subsubsection.3.2.2.2}%
\contentsline {subsubsection}{\numberline {3.2.2.3}Recomendaciones}{12}{subsubsection.3.2.2.3}%
\contentsline {subsubsection}{\numberline {3.2.2.4}Enlaces}{12}{subsubsection.3.2.2.4}%
\contentsline {subsection}{\numberline {3.2.3}Configuración y uso de token JWT}{12}{subsection.3.2.3}%
\contentsline {subsubsection}{\numberline {3.2.3.1}Obtención}{12}{subsubsection.3.2.3.1}%
\contentsline {subsubsection}{\numberline {3.2.3.2}Descripción}{13}{subsubsection.3.2.3.2}%
\contentsline {subsubsection}{\numberline {3.2.3.3}Recomendaciones}{14}{subsubsection.3.2.3.3}%
\contentsline {subsection}{\numberline {3.2.4}Securización durante proceso de login}{14}{subsection.3.2.4}%
\contentsline {subsubsection}{\numberline {3.2.4.1}Obtención}{14}{subsubsection.3.2.4.1}%
\contentsline {subsubsection}{\numberline {3.2.4.2}Descripción}{15}{subsubsection.3.2.4.2}%
\contentsline {subsubsection}{\numberline {3.2.4.3}Recomendaciones}{16}{subsubsection.3.2.4.3}%
\contentsline {subsection}{\numberline {3.2.5}Servicio SSH visible}{16}{subsection.3.2.5}%
\contentsline {subsubsection}{\numberline {3.2.5.1}Obtención}{16}{subsubsection.3.2.5.1}%
\contentsline {subsubsection}{\numberline {3.2.5.2}Descripción}{17}{subsubsection.3.2.5.2}%
\contentsline {subsubsection}{\numberline {3.2.5.3}Recomendaciones}{17}{subsubsection.3.2.5.3}%
\contentsline {subsubsection}{\numberline {3.2.5.4}Enlaces}{17}{subsubsection.3.2.5.4}%
\contentsline {section}{\numberline {3.3}Otras pruebas}{17}{section.3.3}%
\contentsline {subsection}{\numberline {3.3.1}Análisis de caja blanca del código fuente}{18}{subsection.3.3.1}%
\contentsline {subsection}{\numberline {3.3.2}Ataques de diccionario personalizado}{19}{subsection.3.3.2}%
\contentsline {subsubsection}{\numberline {3.3.2.1}Ejecución}{19}{subsubsection.3.3.2.1}%
\contentsline {subsection}{\numberline {3.3.3}Pruebas de rotura de token JWT}{20}{subsection.3.3.3}%
\contentsline {subsubsection}{\numberline {3.3.3.1}Descripción del proceso}{20}{subsubsection.3.3.3.1}%
\contentsline {subsubsection}{\numberline {3.3.3.2}Referencias}{22}{subsubsection.3.3.3.2}%
\contentsline {chapter}{\numberline {4}Anexos}{23}{chapter.4}%
\contentsline {section}{\numberline {4.1}Herramientas}{23}{section.4.1}%
